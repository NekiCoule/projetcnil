extends Button

onready var SkullScene = preload("res://Assets/PirateSkull.tscn")
onready var Map = get_tree().get_root().get_node("MainMap")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _pressed():
	var Skull = SkullScene.instance()
	
	get_parent().visible = false
	Map.add_child(Skull)
	Skull.global_position = Map.get_node("PlayerPosition").global_position
	Skull.global_translate(Vector2(0, -50))
	
	get_parent().queue_free()