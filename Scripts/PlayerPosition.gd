extends Node2D

export var MaxHealth = 10
var Health
var HealthPotions = 0
export(String) var WeaponEquiped = "Hands"
export(float) var Damage = 1
export(Vector2) var spawnPosition = Vector2(0,0)


func _ready():
	ResetHealth()
	ResetCamera()

func MoveCamera(MoveVector):
	self.global_translate(MoveVector)
	print(str(self.position) + " Removve this when done Max!!")
	
func setCamera(vectorPosition):
	self.global_position = vectorPosition
	
func ResetCamera():
	setCamera(spawnPosition)
	
	
func setWeapon(WeaponName, WeaponDamage):
	WeaponEquiped = WeaponName
	Damage = WeaponDamage
	# print(Damage)
	
func getDamage():
	return Damage
	
func hit(damageDealt):
	Health = Health - damageDealt

func heal(healingDone):
	if Health+healingDone > MaxHealth:
		var newHealing = MaxHealth - Health
		Health = Health + newHealing
		print("You have been healed from " + str(newHealing) + " damages (lost " + str(healingDone - newHealing) + ")")
	else:
		Health = Health + healingDone
		print("You have been healed from " + str(healingDone) + " damages")
	print("Your health: " + str(Health))

func enoughPotions():
	if HealthPotions > 0:
		return true
	else:
		print("Not enough potions")
		return false

func ResetHealth():
	Health = MaxHealth
	
func givePotion(quantity):
	HealthPotions = HealthPotions + quantity
	
func getHealthPotions():
	return HealthPotions
	
func dead():
	if Health <= 0:
		print("You have been defeated!")
		return true
	else:
		return false