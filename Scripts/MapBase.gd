extends Node2D

var clicked = []
var onCombat = false

func _ready():
	set_process(false)

func _process(delta):
    set_process(false)
    
    var topmost_clicked = clicked.front()
    for i in clicked:
        if  i.get_index() > topmost_clicked.get_index():
            topmost_clicked = i
    clicked.clear()
    if inCombat() and not topmost_clicked.CombatClickable:
        print("In combat")
    else:
        topmost_clicked.onClick()

func EnterCombat():
	onCombat = true
	
func LeaveCombat():
	onCombat = false

func inCombat():
	return onCombat