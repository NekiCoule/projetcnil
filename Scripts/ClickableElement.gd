extends Area2D

var Map
var Character
var CombatClickable = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Map = get_tree().get_root().get_node("MainMap")
	Character = Map.get_node("PlayerPosition")

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
    and event.button_index == BUTTON_LEFT \
	and event.is_pressed():
		
		self.CheckClick()

func CheckClick():
    Map.clicked.append(self)
    Map.set_process(true)
	
func onClick():
	print("Clickable element clicked")