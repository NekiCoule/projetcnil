extends "res://Scripts/Collectible.gd"

export(String) var Name = ""
export(float) var Damage = 1

func _ready():
	pass
	
func onCollect():
	Character.setWeapon(Name, Damage)
	print("Picked up a " + Name)