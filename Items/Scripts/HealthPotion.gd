extends "res://Scripts/Collectible.gd"

export(float) var Heal = 5

func _ready():
	pass 
	
func onCollect():
	Character.givePotion(1)
	print("You got a healing potion (" + str(Character.getHealthPotions()) + ")")
