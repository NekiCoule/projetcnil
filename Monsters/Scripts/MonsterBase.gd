extends "res://Scripts/ClickableElement.gd"

onready var CombatPanelScene = preload("res://Buttons/CombatPanel.tscn")

export(float) var MaxHealth = 1
var Health = 1
export(float) var Damage = 1
export(bool) var left_right = true

var basePosition


func _ready():
	basePosition = self.global_position
	

func _process(delta):
	if left_right:
		if Character.position.y == self.position.y:
			if Character.position.x == self.position.x - 530:
				self.translate(Vector2(-100,0))
			elif Character.position.x == self.position.x + 530:
				self.translate(Vector2(100,0))
	else:
		if Character.position.x == self.position.x:
			if Character.position.y == self.position.y - 360:
				self.translate(Vector2(0,-180))
			elif Character.position.y == self.position.y + 360:
				self.translate(Vector2(0,180))
				
					
func onClick():
	
	Map.EnterCombat()
	
	print(str(Character.position) + " " + str(self.position) + " " + str(self.position.y + 190))
	
	if left_right:
		if Character.position.y == self.position.y:
			if Character.position.x == self.position.x - 430:
				self.translate(Vector2(-170,0))
			elif Character.position.x == self.position.x + 430:
				self.translate(Vector2(170,0))
	else:
		if Character.position.x == self.position.x:
			if Character.position.y == self.position.y - 180:
				self.translate(Vector2(0,-150))
			elif Character.position.y == self.position.y + 180:
				self.translate(Vector2(0,50))
	
	
	
	var CombatPanel = CombatPanelScene.instance()
	add_child(CombatPanel)
	CombatPanel.set_global_position(Character.get_position())
	
		
	
	# print(CombatPanel.get_position())
	
func ResetHealth():
	Health = MaxHealth

func getDamage():
	return Damage

func hit(damageDealt):
	Health = Health - damageDealt
	
func resetPosition():
	self.set_global_position(basePosition)

func dead():
	if Health <= 0:
		print("Monster defeated!")
		return true
	else:
		return false